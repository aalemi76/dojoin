//
//  TableViewContainer.swift
//  FindIt
//
//  Created by Catalina on 9/12/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class TableViewContainer: UIView, TableViewProvider {
    var sections: [Sectionable] = []
    var tableView: UITableView
    var dataSourceHandler: TableViewDataSourceHandler
    var delegateHandler: TableViewDelegateHandler
    required init(tableView: UITableView) {
        self.tableView = tableView
        dataSourceHandler = TableViewDataSourceHandler(sections: sections)
        delegateHandler = TableViewDelegateHandler(sections: sections)
        super.init(frame: .zero)
        addSubview(self.tableView)
        self.tableView.pinToEdge(self)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
