//
//  TableCellViewModel.swift
//  FindIt
//
//  Created by a.alami on 22/10/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
class TableCellViewModel: Reusable {
    let reuseID: String
    let cellClass: AnyClass
    var model: Any
    required init(reuseID: String, cellClass: AnyClass, model: Any) {
        self.reuseID = reuseID
        self.cellClass = cellClass
        self.model = model
    }
    func getReuseID() -> String { return reuseID}
    func getCellClass() -> AnyClass { return cellClass }
    func getModel() -> Any { return model }
    func updateModel(model: Any) {
        self.model = model
    }
    func cellDidLoad(_ cell: Updatable) {
        return
    }
}
