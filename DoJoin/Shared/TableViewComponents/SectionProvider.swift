//
//  SectionProvider.swift
//  FindIt
//
//  Created by Catalina on 9/18/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UIView
class SectionProvider: Sectionable {
    private var cells: [Reusable]
    private var headerView: UIView?
    private var footerView: UIView?
    required init(cells: [Reusable], headerView: UIView?, footerView: UIView?) {
        self.cells = cells
        self.headerView = headerView
        self.footerView = footerView
    }
    func getCells() -> [Reusable] { return cells }
    func getHeaderView() -> UIView? { return headerView }
    func getFooterView() -> UIView? { return footerView }
    func append(_ cells: [Reusable]) {
        self.cells += cells
    }
    func removeCells() {
        cells.removeAll()
    }
}
