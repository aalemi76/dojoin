//
//  TableViewProvider.swift
//  HamrahCard
//
//  Created by Catalina on 11/15/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UITableView
protocol TableViewProvider: class  {
    var sections: [Sectionable] { get set }
    var tableView: UITableView { get }
    var dataSourceHandler: TableViewDataSourceHandler { get set }
    var delegateHandler: TableViewDelegateHandler { get set }
    init(collectionView: UICollectionView)
    func registerSections(_ sections: [Sectionable])
    func load(_ sections: [Sectionable], dataSourceHandler: TableViewDataSourceHandler?, delegateHandler: TableViewDelegateHandler?)
    func reloadItems(at indexPaths: [IndexPath])
    func reloadSections(sections: IndexSet)
    func loadNextPage(_ items: [Reusable], from indexPath: IndexPath)
}
extension TableViewProvider {
    func registerSections(_ sections: [Sectionable]) {
        sections.forEach { (section) in
            section.getCells().forEach { (reusable) in
                let nibCell = UINib(nibName: reusable.getReuseID(), bundle: nil)
                tableView.register(nibCell, forCellReuseIdentifier: reusable.getReuseID())
            }
        }
    }
    func load(_ sections: [Sectionable], dataSourceHandler: TableViewDataSourceHandler?, delegateHandler: TableViewDelegateHandler?) {
        registerSections(sections)
        self.sections = sections
        if let dataSource = dataSourceHandler {
            tableView.dataSource = dataSource
        } else {
            self.dataSourceHandler =  TableViewDataSourceHandler(sections: sections)
            tableView.dataSource = self.dataSourceHandler
        }
        if let delegate = delegateHandler {
            tableView.delegate = delegate
        } else {
            self.delegateHandler = TableViewDelegateHandler(sections: sections)
            tableView.delegate = self.delegateHandler
        }
        tableView.reloadData()
    }
    func reloadItems(at indexPaths: [IndexPath]) {
        tableView.reloadRows(at: indexPaths, with: .automatic)
    }
    func reloadSections(sections: IndexSet) {
        tableView.reloadSections(sections, with: .automatic)
    }
    func loadNextPage(_ items: [Reusable], from indexPath: IndexPath) {
        let lastSection = indexPath.section
        let lastRow = indexPath.row + 1
        let indexPaths = items.enumerated().map({ IndexPath(row: lastRow + $0.offset, section: lastSection) })
        sections[lastSection].append(items)
        tableView.insertRows(at: indexPaths, with: .automatic)
    }
}
