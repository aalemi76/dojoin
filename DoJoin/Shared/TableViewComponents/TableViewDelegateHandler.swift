//
//  TableViewDelegateHandler.swift
//  FindIt
//
//  Created by Catalina on 10/16/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UITableView
import RxSwift
class TableViewDelegateHandler: NSObject, UITableViewDelegate {
    private var sections: [Sectionable]
    var passSelectedItem = PublishSubject<Any>()
    var passSelectedSection = PublishSubject<Int>()
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = sections[section].getHeaderView()
        return header
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = sections[section].getFooterView()
        return footer
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = sections[indexPath.section].getCells()[indexPath.row].getModel()
        passSelectedItem.onNext(item)
    }
}
