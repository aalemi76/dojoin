//
//  SharedViewController.swift
//  FindIt
//
//  Created by Catalina on 9/12/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UIViewController
import RxSwift
class SharedViewController: UIViewController {
    let disposeBag = DisposeBag()
    weak var infoView: InformationView?
    lazy var loadingView: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.color = GlobalSettings.shared().mainColor
        spinner.backgroundColor = GlobalSettings.shared().lightGreen
        spinner.bounds = CGRect(x: 0, y: 0, width: 80, height: 80)
        spinner.layer.cornerRadius = 20
        spinner.layer.masksToBounds = true
        spinner.layer.borderWidth = 1
        spinner.layer.borderColor = GlobalSettings.shared().mainColor.cgColor
        spinner.hidesWhenStopped = true
        return spinner
    }()
    func showLoadingView() {
        dismissLoadingView()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.view.addSubview(self.loadingView)
            self.loadingView.center = self.view.center
            UIView.animate(withDuration: 2, delay: 0.5, options: [.curveEaseInOut], animations: {
                self.loadingView.startAnimating()
            })
        }
    }
    func dismissLoadingView() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.loadingView.stopAnimating()
            self.loadingView.removeFromSuperview()
        }
    }
    func showSuccessBanner(title: String) {
        infoView?.removeFromSuperview()
        infoView = InformationView()
        DispatchQueue.main.async { [weak self] in
            self?.infoView?.setInformation(description: title, type: .success)
        }
    }
    func showErrorBanner(title: String) {
        infoView?.removeFromSuperview()
        infoView = InformationView()
        DispatchQueue.main.async { [weak self] in
            self?.infoView?.setInformation(description: title, type: .failure)
        }
    }
}
