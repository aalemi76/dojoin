//
//  DoJoinNavigationController.swift
//  FindIt
//
//  Created by a.alami on 07/09/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UINavigationController
class DoJoinNavigationController: UINavigationController, UIGestureRecognizerDelegate {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        interactivePopGestureRecognizer?.delegate = self
    }
    func configureView() {
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: GlobalSettings.shared().systemFont(type: .bold, size: 18)]
        navigationBar.titleTextAttributes = textAttributes
        navigationBar.barTintColor = GlobalSettings.shared().lightGray
        navigationBar.tintColor = GlobalSettings.shared().mainColor
        navigationBar.shadowImage = UIImage()
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
}
