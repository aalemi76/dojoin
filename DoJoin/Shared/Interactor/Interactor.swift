//
//  LoginInteractor.swift
//  FindIt
//
//  Created by Catalina on 9/11/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
class Interactor<Model: Codable>: Parsable {
    typealias Object = Model
    func parse(data: Data) -> Object? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let model = try? decoder.decode(GeneralModel<Object>.self, from: data)
        return model?.result
    }
    func getModel(url: String, parameters: [String: Any]?,
                  onSuccess: @escaping (Object) -> Void,
                  onFailure: @escaping (DoJoinError) -> Void) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            NetworkManager.shared.request(url, method: .get, parameters: parameters) { [weak self] result in
                switch result {
                case .success(let data):
                    guard let model = self?.parse(data: data) else {
                        onFailure(.onParseError)
                        return
                    }
                    onSuccess(model)
                case .failure(let error):
                    onFailure(error)
                }
            }
        }
    }
}
