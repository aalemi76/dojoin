//
//  Parsable.swift
//  FindIt
//
//  Created by Catalina on 9/11/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
protocol Parsable {
    associatedtype Object: Codable
    func parse(data: Data) -> Object?
}
