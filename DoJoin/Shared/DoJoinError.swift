//
//  DoJoinError.swift
//  DoJoin
//
//  Created by a.alami on 06/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
enum DoJoinError: String, Error {
    case onServerError = "Server is unavailable, please try again."
    case onParseError = "An Error occured while parsing the data."
    case unKnownError = "Unexpected error happend"
}
