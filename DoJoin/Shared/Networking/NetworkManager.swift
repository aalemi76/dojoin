//
//  NetworkManager.swift
//  FindIt
//
//  Created by Catalina on 9/10/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Alamofire
class NetworkManager {
    static let shared: NetworkManager = {
        return NetworkManager()
    }()
    typealias completionHandler = ((Result<Data, DoJoinError>) -> Void)
    typealias RequestModifier = (inout URLRequest) throws -> Void
    var request: Alamofire.Request?
    var retryLimit: Int = 3
    func request(_ url: String, method: HTTPMethod = .get, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.queryString, headers: HTTPHeaders? = nil, interceptor: RequestInterceptor? = nil, requestModifier: RequestModifier? = nil, completion: @escaping completionHandler) {
        AF.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers, interceptor: interceptor, requestModifier: requestModifier).validate().responseJSON { (response) in
            if let data = response.data {
                completion(.success(data))
            } else {
                completion(.failure(DoJoinError.onServerError))
            }
        }
    }
}
