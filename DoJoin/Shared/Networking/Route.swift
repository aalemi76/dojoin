//
//  APIManager.swift
//  FindIt
//
//  Created by Catalina on 9/10/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
enum Route: String {
    case baseURL = "http://208.109.13.111:9090/"
    case type = "api/"
    case category = "Category"
    static func generate(_ route: Route) -> String {
        return Route.baseURL.rawValue + Route.type.rawValue + route.rawValue
    }
}
