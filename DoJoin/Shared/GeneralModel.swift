//
//  GeneralModel.swift
//  DoJoin
//
//  Created by a.alami on 06/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
struct GeneralModel<Object: Codable>: Codable {
    let result: Object?
    let errorMessage: String?
    let paging: String?
    let serverTime: String
}
