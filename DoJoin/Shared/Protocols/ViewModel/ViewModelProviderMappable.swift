//
//  ViewModelProviderMappable.swift
//  FindIt
//
//  Created by Catalina on 9/18/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
protocol ViewModelProviderMappable: ViewModelProvider {
    associatedtype model: Codable
    init(interactor: Interactor<model>)
}
