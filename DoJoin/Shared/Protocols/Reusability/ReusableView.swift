//
//  ReusableView.swift
//  FindIt
//
//  Created by Catalina on 9/18/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
protocol ReusableView {
    func getReuseID() -> String
    func getViewClass() -> AnyClass
}
