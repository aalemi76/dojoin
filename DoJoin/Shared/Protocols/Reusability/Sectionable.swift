//
//  Sectionable.swift
//  FindIt
//
//  Created by Catalina on 9/18/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UIView
protocol Sectionable: class {
    init(cells: [Reusable], headerView: UIView?, footerView: UIView?)
    func getCells() -> [Reusable]
    func getHeaderView() -> UIView?
    func getFooterView() -> UIView?
    func append(_ cells: [Reusable])
    func removeCells()
}
