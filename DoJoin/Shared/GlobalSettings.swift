//
//  GlobalSettings.swift
//  FindIt
//
//  Created by a.alami on 07/09/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
final class GlobalSettings: NSObject {
    private static let sharedIntance: GlobalSettings = {
        return GlobalSettings()
    }()
    static func shared() -> GlobalSettings {
        return sharedIntance
    }
    //MARK:- Colors
    let mainColor = UIColor(displayP3Red: 9/255, green: 158/255, blue: 134/255, alpha: 1)
    let lightGray = UIColor(displayP3Red: 164/255, green: 182/255, blue: 181/255, alpha: 1)
    let darkGray = UIColor(displayP3Red: 66/255, green: 84/255, blue: 88/255, alpha: 1)
    let lightBlack = UIColor(displayP3Red: 15/255, green: 16/255, blue: 15/255, alpha: 1)
    let lightBlue = UIColor(displayP3Red: 86/255, green: 195/255, blue: 189/255, alpha: 1)
    let lightGreen = UIColor(displayP3Red: 218/255, green: 220/255, blue: 169/255, alpha: 1)
    let lightRed = UIColor(displayP3Red: 212/255, green: 65/255, blue: 88/255, alpha: 1)
    //MARK:- Fonts
    private func calculateFontSize() -> CGFloat {
        let fontSize : CGFloat = 13;
        var newFontSize : CGFloat = UIScreen.main.bounds.size.height * CGFloat(fontSize/568)
        if (UIScreen.main.bounds.size.height < 500) {
            newFontSize = UIScreen.main.bounds.size.height * CGFloat(CGFloat(fontSize) / 480.0);
        }
        let maxFontSize : CGFloat = 18;
        if (newFontSize>maxFontSize) {
            newFontSize = maxFontSize;
        }
        return newFontSize;
    }
    func systemFont(type: FontType = .regular) -> UIFont {
        return UIFont(name: type.rawValue, size:self.calculateFontSize())!
    }
    func systemFont(type: FontType = .regular, size: CGFloat) -> UIFont {
        return UIFont.init(name: type.rawValue, size:size)!
    }
    enum FontType: String {
        case light = "NunitoSans-Light"
        case regular = "NunitoSans-Regular"
        case semiBold = "NunitoSans-SemiBold"
        case bold = "NunitoSans-Bold"
    }
}
