//
//  ConfirmCellViewModel.swift
//  DoJoin
//
//  Created by a.alami on 07/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
protocol ConfirmCellViewModelDelegate: class {
    func didTapNextButton()
}
class ConfirmCellViewModel: TableCellViewModel {
    weak var delegate: ConfirmCellViewModelDelegate?
    func didTapNextButton() {
        delegate?.didTapNextButton()
    }
}
