//
//  ConfirmTableViewCell.swift
//  DoJoin
//
//  Created by a.alami on 07/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class ConfirmTableViewCell: UITableViewCell, Updatable {
    static let reuseID = String(describing: ConfirmTableViewCell.self)
    weak var viewModel: ConfirmCellViewModel?
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton! {
        didSet {
            nextButton.layer.masksToBounds = true
            nextButton.layer.cornerRadius = 5
        }
    }
    @IBAction func didTapNextButton() {
        viewModel?.didTapNextButton()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel as? ConfirmCellViewModel
    }
    func update(model: Any) {
        hintLabel.text = model as? String
    }
}
