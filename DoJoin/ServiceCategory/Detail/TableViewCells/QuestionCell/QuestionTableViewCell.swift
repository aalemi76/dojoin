//
//  QuestionTableViewCell.swift
//  DoJoin
//
//  Created by a.alami on 07/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
class QuestionTableViewCell: UITableViewCell, Updatable {
    static let reuseID = String(describing: QuestionTableViewCell.self)
    let disposeBag = DisposeBag()
    private weak var viewModel: QuestionCellViewModel?
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var upChoiceBtn: UIButton!
    @IBOutlet weak var downChoiceBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel as? QuestionCellViewModel
    }
    func update(model: Any) {
        guard let model = model as? InfoModel else { return }
        hintLabel.text = model.hint
        questionLabel.text = model.question
        upChoiceBtn.setTitle("  \(model.choices[0].title)", for: .normal)
        upChoiceBtn.isEnabled = model.choices[0].isAvailable
        if !model.choices[0].isAvailable {
            upChoiceBtn.setTitleColor(.lightGray, for: .normal)
        }
        upChoiceBtn.rx.tap.bind { [weak self] (_) in
            self?.upChoiceBtn.setImage(UIImage(systemName: "circle.fill"), for: .normal)
            self?.downChoiceBtn.setImage(UIImage(systemName: "circle"), for: .normal)
            self?.viewModel?.didTapChoice(withTitle: model.choices[0].title)
        }.disposed(by: disposeBag)
        downChoiceBtn.setTitle("  \(model.choices[1].title)", for: .normal)
        downChoiceBtn.isEnabled = model.choices[1].isAvailable
        if !model.choices[1].isAvailable {
            downChoiceBtn.setTitleColor(.lightGray, for: .normal)
        }
        downChoiceBtn.rx.tap.bind { [weak self] (_) in
            self?.downChoiceBtn.setImage(UIImage(systemName: "circle.fill"), for: .normal)
            self?.upChoiceBtn.setImage(UIImage(systemName: "circle"), for: .normal)
            self?.viewModel?.didTapChoice(withTitle: model.choices[1].title)
        }.disposed(by: disposeBag)
    }
}
