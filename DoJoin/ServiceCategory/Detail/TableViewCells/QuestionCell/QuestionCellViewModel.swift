//
//  QuestionCellViewModel.swift
//  DoJoin
//
//  Created by a.alami on 07/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
protocol QuestionCellViewModelDelegate: class {
    func didTapChoice(withTitle title: String)
}
class QuestionCellViewModel: TableCellViewModel {
    weak var delegate: QuestionCellViewModelDelegate?
    func didTapChoice(withTitle title: String) {
        delegate?.didTapChoice(withTitle: title)
    }
}
