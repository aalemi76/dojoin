//
//  GeneralInfoViewController+Ext.swift
//  DoJoin
//
//  Created by a.alami on 07/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
extension GeneralInfoViewController: Viewable {
    func show(result: Result<Any, DoJoinError>) {
        DispatchQueue.main.async { [weak self] in
            switch result {
            case .success(let reusableCells):
                guard let sections = reusableCells as? [Sectionable] else {
                    self?.showErrorBanner(title: DoJoinError.unKnownError.rawValue)
                    return
                }
                self?.tableViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
            case .failure(let error):
                self?.showErrorBanner(title: error.rawValue)
            }
        }
    }
}
