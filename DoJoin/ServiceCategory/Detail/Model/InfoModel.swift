//
//  InfoModel.swift
//  DoJoin
//
//  Created by a.alami on 07/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
struct InfoModel {
    let hint: String?
    let question: String
    let choices: [Choice]
}
struct Choice {
    let title: String
    let isAvailable: Bool
}
