//
//  GeneralInfoViewModel+Ext.swift
//  DoJoin
//
//  Created by a.alami on 07/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
extension GeneralInfoViewModel: QuestionCellViewModelDelegate {
    func didTapChoice(withTitle title: String) {
        switch title {
        case "In-Person":
            prepareAppointmentCell()
        case "Book a time":
            updateThirdCell(isBook: true)
        case  "Drop in":
            updateThirdCell(isBook: false)
        default:
            return
        }
    }
    func prepareAppointmentCell() {
        guard sections[0].getCells().count == 1 else { return }
        let item = QuestionCellViewModel(reuseID: QuestionTableViewCell.reuseID, cellClass: QuestionTableViewCell.self, model: questionModels[1])
        item.delegate = self
        (view as? GeneralInfoViewController)?.tableViewContainer.loadNextPage([item], from: IndexPath(row: 0, section: 0))
    }
    func updateThirdCell(isBook: Bool) {
        sections[0].getCells().count == 3 ? updateHint(isBook: isBook) : addThirdCell(isBook: isBook)
    }
    func updateHint(isBook: Bool) {
        let hint = isBook ? "No need for pre-registration, people can stop by your business at any of the available times." : "Set a work schedule and people will need to book any of the available time slots to meet you."
        sections[0].getCells()[2].updateModel(model: hint)
        (view as? GeneralInfoViewController)?.tableViewContainer.reloadItems(at: [IndexPath(row: 2, section: 0)])
    }
    func addThirdCell(isBook: Bool) {
        let hint = isBook ? "No need for pre-registration, people can stop by your business at any of the available times." : "Set a work schedule and people will need to book any of the available time slots to meet you."
        let item = ConfirmCellViewModel(reuseID: ConfirmTableViewCell.reuseID, cellClass: ConfirmTableViewCell.self, model: hint)
        item.delegate = self
        (view as? GeneralInfoViewController)?.tableViewContainer.loadNextPage([item], from: IndexPath(row: 1, section: 0))
    }
}
extension GeneralInfoViewModel: ConfirmCellViewModelDelegate {
    func didTapNextButton() {
        print("To Schedule Page")
    }
}
