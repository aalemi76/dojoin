//
//  GeneralInfoViewModel.swift
//  DoJoin
//
//  Created by a.alami on 07/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
class GeneralInfoViewModel: ViewModelProvider {
    weak var view: Viewable?
    var sections = [Sectionable]()
    var questionModels = [InfoModel]()
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        makeQuestionModels()
        prepareSections()
        self.view?.show(result: .success(sections))
    }
    func makeQuestionModels() {
        let contactChoices = [Choice(title: "In-Person", isAvailable: true), Choice(title: "Online (coming soon)", isAvailable: false)]
        let appointmentChoices = [Choice(title: "Book a time", isAvailable: true), Choice(title: "Drop in", isAvailable: true)]
        questionModels = [InfoModel(hint: nil, question: "How people may reach you?", choices: contactChoices), InfoModel(hint: "Your services are presented locally. People will either come to your store or meet you in their own places.", question: "How to make appointments with you?", choices: appointmentChoices)]
    }
    func prepareSections() {
        let item = QuestionCellViewModel(reuseID: QuestionTableViewCell.reuseID, cellClass: QuestionTableViewCell.self, model: questionModels[0])
        item.delegate = self
        sections = [SectionProvider(cells: [item], headerView: nil, footerView: nil)]
    }
}
