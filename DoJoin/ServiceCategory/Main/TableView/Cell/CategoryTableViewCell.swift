//
//  CategoryTableViewCell.swift
//  DoJoin
//
//  Created by a.alami on 07/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class CategoryTableViewCell: UITableViewCell, Updatable {
    static let reuseID = String(describing: CategoryTableViewCell.self)
    @IBOutlet weak var label: UILabel! {
        didSet {
            label.font = GlobalSettings.shared().systemFont(type: .light, size: 12)
            label.tintColor = GlobalSettings.shared().darkGray
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func attach(viewModel: Reusable) {
        return
    }
    func update(model: Any) {
        guard let model = model as? SubCategory else { return }
        label.text = model.title
    }
}
