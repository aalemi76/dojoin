//
//  CategorySectionView.swift
//  DoJoin
//
//  Created by a.alami on 06/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UIView
import RxSwift
import RxCocoa
class CategorySectionView: UIView {
    let title: String
    var isExpanded = false
    let publishExpandState = PublishSubject<Bool>()
    let disposeBag = DisposeBag()
    init(title: String) {
        self.title = title
        super.init(frame: .zero)
        layoutView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private lazy var button: UIButton = {
        let btn = UIButton(type: .system)
        btn.contentHorizontalAlignment = .left
        btn.setImage(UIImage(systemName: "chevron.right"), for: .normal)
        btn.setTitle("  \(title)", for: .normal)
        btn.titleLabel?.font = GlobalSettings.shared().systemFont(type: .semiBold, size: 14)
        btn.tintColor = GlobalSettings.shared().darkGray
        return btn
    }()
    private lazy var border: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        return view
    }()
    private func layoutView() {
        addButton()
        addBorder()
    }
    private func addButton() {
        button.translatesAutoresizingMaskIntoConstraints = false
        addSubview(button)
        NSLayoutConstraint.activate([
            button.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            button.centerYAnchor.constraint(equalTo: centerYAnchor),
            button.widthAnchor.constraint(equalTo: widthAnchor, constant: -20)])
        button.rx.tap.bind { [weak self] _ in
            guard let self = self else { return }
            self.isExpanded = !self.isExpanded
            self.isExpanded ? self.button.setImage(UIImage(systemName: "chevron.down"), for: .normal) : self.button.setImage(UIImage(systemName: "chevron.right"), for: .normal)
            self.publishExpandState.onNext(self.isExpanded)
        }.disposed(by: disposeBag)
    }
    private func addBorder() {
        border.translatesAutoresizingMaskIntoConstraints = false
        addSubview(border)
        NSLayoutConstraint.activate([
            border.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 3),
            border.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -3),
            border.bottomAnchor.constraint(equalTo: bottomAnchor),
            border.heightAnchor.constraint(equalToConstant: 1)])
    }
}
