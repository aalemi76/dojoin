//
//  ServiceCategoryViewModel.swift
//  DoJoin
//
//  Created by a.alami on 06/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
import RxSwift
class ServiceCategoryViewModel: ViewModelProviderMappable {
    typealias model = [Category]
    private let interactor: Interactor<[Category]>
    var sections = [Sectionable]()
    let disposeBag = DisposeBag()
    private weak var view: Viewable?
    required init(interactor: Interactor<[Category]>) {
        self.interactor = interactor
    }
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        getModel()
    }
    func getModel() {
        let url = Route.generate(.category)
        interactor.getModel(url: url, parameters: nil, onSuccess: { [weak self] (categories) in
            guard let self = self else { return }
            self.prepareSections(categories: categories)
            self.view?.show(result: .success(self.sections))
        }) { (error) in
            self.view?.show(result: .failure(error))
        }
    }
    func prepareSections(categories: [Category]) {
        sections = categories.enumerated().map { [weak self] (offset, category) -> Sectionable in
            let header =  CategorySectionView(title: category.title)
            self?.listenForSectionState(section: header, subCategories: category.subCategories, index: offset)
            return SectionProvider(cells: [], headerView: header, footerView: nil)
        }
    }
    func listenForSectionState(section: CategorySectionView, subCategories: [SubCategory], index: Int) {
        section.publishExpandState.subscribe { [weak self] (event) in
            guard let self = self, let isExpanded = event.element else { return }
            if isExpanded {
                let items = subCategories.map({ TableCellViewModel(reuseID: CategoryTableViewCell.reuseID, cellClass: CategoryTableViewCell.self, model: $0 )})
                (self.view as? ServiceCategoryViewController)?.expandSection(items: items, index: index)
            } else {
                (self.view as? ServiceCategoryViewController)?.closeSection(index: index)
            }
        }.disposed(by: disposeBag)
    }
}
