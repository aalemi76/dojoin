//
//  Category.swift
//  DoJoin
//
//  Created by a.alami on 06/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
struct Category: Codable {
    let id, title: String
    let subCategories: [SubCategory]
}
struct SubCategory: Codable {
let id, title: String
}
