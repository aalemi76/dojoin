//
//  ServiceCategoryViewController.swift
//  DoJoin
//
//  Created by a.alami on 06/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
protocol ServiceCategoryViewControllerDelegate: class {
    func didTapOnCell(title: String)
}
class ServiceCategoryViewController: SharedViewController {
    weak var delegate: ServiceCategoryViewControllerDelegate?
    let viewModel: ServiceCategoryViewModel
    private lazy var titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Select your service category & profession"
        lbl.font = GlobalSettings.shared().systemFont(type: .bold, size: 18)
        lbl.textColor = GlobalSettings.shared().darkGray
        lbl.textAlignment = .left
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 0
        return lbl
    }()
    private lazy var tableView: UITableView = {
        let tbl = UITableView(frame: .zero, style: .plain)
        tbl.sectionHeaderHeight = 44
        tbl.sectionFooterHeight = 0
        tbl.rowHeight = 44
        tbl.tableFooterView = UIView()
        return tbl
    }()
    lazy var tableViewContainer: TableViewContainer = {
        let container = TableViewContainer(tableView: tableView)
        return container
    }()
    init(viewModel: ServiceCategoryViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutView()
        viewModel.viewDidLoad(self)
    }
    func layoutView() {
        view.backgroundColor = .white
        addTitleLabel()
        addTableView()
    }
    func addTitleLabel() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: view.trailingAnchor, constant:  -10)])
    }
    func addTableView() {
        tableViewContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableViewContainer)
        NSLayoutConstraint.activate([
            tableViewContainer.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
            tableViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
}
