//
//  ServiceCategoryViewController+Ext.swift
//  DoJoin
//
//  Created by a.alami on 06/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
extension ServiceCategoryViewController: Viewable {
    func show(result: Result<Any, DoJoinError>) {
        DispatchQueue.main.async { [weak self] in
            switch result {
            case .success(let reusableCells):
                guard let sections = reusableCells as? [Sectionable] else {
                    self?.showErrorBanner(title: DoJoinError.unKnownError.rawValue)
                    return
                }
                self?.configureTableView(sections)
            case .failure(let error):
                self?.showErrorBanner(title: error.rawValue)
            }
        }
    }
    func configureTableView(_ sections: [Sectionable]) {
        DispatchQueue.main.async { [weak self] in
            self?.tableViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
            self?.subscribeOnTap()
        }
    }
    func subscribeOnTap() {
        tableViewContainer.delegateHandler.passSelectedItem.subscribe { [weak self] (event) in
            guard let self = self, let model = event.element as? SubCategory else { return }
            self.delegate?.didTapOnCell(title: model.title)
        }.disposed(by: disposeBag)
    }
    func expandSection(items: [Reusable], index: Int) {
        tableViewContainer.addSubCells(items, for: index)
    }
    func closeSection(index: Int) {
        tableViewContainer.removeSubCells(for: index)
    }
}
