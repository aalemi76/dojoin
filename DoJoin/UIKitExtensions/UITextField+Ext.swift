//
//  UITextField+Ext.swift
//  FindIt
//
//  Created by Catalina on 9/12/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
extension UITextField {
    func isEmpty() -> Bool {
        let text = removeWhiteSpaces()
        return text == ""
    }
    func removeWhiteSpaces() -> String {
        guard let text = text else { return "" }
        return text.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    func getRawText(option: CharacterSet) -> String? {
        return text?.trimmingCharacters(in: option)
    }
}
