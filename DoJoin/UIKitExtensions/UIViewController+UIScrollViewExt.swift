//
//  UIViewController+UIScrollViewExt.swift
//  FindIt
//
//  Created by a.alami on 08/09/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
extension UIViewController {
    @discardableResult func setScrollView(relaviteToSafeArea: Bool = true) -> UIScrollView {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .clear
        scrollView.layoutIfNeeded()
        scrollView.sizeToFit()
        view.addSubview(scrollView)
        scrollView.pinToEdge(view)
        return scrollView
    }
    func setContentView(for scrollView: UIScrollView, contentView: UIView, isLogin: Bool = false) {
        contentView.backgroundColor = .clear
        contentView.sizeToFit()
        scrollView.addSubview(contentView)
        contentView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        if !isLogin {
            contentView.pinToEdge(scrollView)
            let heightAnchor = contentView.heightAnchor.constraint(greaterThanOrEqualTo: view.heightAnchor)
            heightAnchor.priority = UILayoutPriority(rawValue: 249)
            heightAnchor.isActive = true
        } else {
            contentView.pinToCenter(scrollView, centerXConstant: 0, centerYConstant: 20, widthSize: 0, heightSize: 225, isWidthRelative: true, isHeightRelative: false)
        }
    }
    @objc func viewDidTap(tapGestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    func hideKeyboardOnTap() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewDidTap(tapGestureRecognizer:)))
        tapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGestureRecognizer)
    }
}
