//
//  UIScrollView+Ext.swift
//  FindIt
//
//  Created by Catalina on 9/8/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
extension UIScrollView {
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardViewEndFrame = convert(keyboardSize, from: window)
            contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 70, right: 0)
        }
        scrollIndicatorInsets = contentInset
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        contentInset = .zero
        scrollIndicatorInsets = contentInset
    }
    func listenForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}
