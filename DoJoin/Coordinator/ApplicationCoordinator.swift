//
//  ApplicationCoordinator.swift
//  FindIt
//
//  Created by Catalina on 9/18/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UIViewController
class ApplicationCoordinator {
    let window: UIWindow
    init(window: UIWindow) {
        self.window = window
    }
    func handleUserState(windowScene: UIWindowScene) -> Coordinator {
        let coordinator = ServiceCategoryCoordintor(navigationController: DoJoinNavigationController())
        setRootViewcontroller(viewController: coordinator.navigationController, windowScene: windowScene)
        return coordinator
    }
    private func setRootViewcontroller(viewController: UIViewController, windowScene: UIWindowScene) {
        window.windowScene = windowScene
        window.rootViewController = viewController
        window.makeKeyAndVisible()
    }
}
