//
//  ServiceCategoryCoordintor.swift
//  DoJoin
//
//  Created by a.alami on 06/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UINavigationController
class ServiceCategoryCoordintor: Coordinator {
    var childCoordinators: [Coordinator]
    var navigationController: UINavigationController
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = []
    }
    func start() {
        let viewModel = ServiceCategoryViewModel(interactor: Interactor<[Category]>())
        let viewController = ServiceCategoryViewController(viewModel: viewModel)
        viewController.delegate = self
        viewController.navigationItem.title = "Service Category"
        navigationController.pushViewController(viewController, animated: true)
        navigationController.setNavigationBarHidden(false, animated: true)
    }
}
extension ServiceCategoryCoordintor: ServiceCategoryViewControllerDelegate {
    func didTapOnCell(title: String) {
        let viewModel = GeneralInfoViewModel()
        let viewController = GeneralInfoViewController(viewModel: viewModel)
        viewController.navigationItem.title = "General Information"
        navigationController.pushViewController(viewController, animated: true)
        navigationController.setNavigationBarHidden(false, animated: true)
    }
}
