//
//  SceneDelegate.swift
//  DoJoin
//
//  Created by a.alami on 06/12/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    var coordinator: Coordinator?
    var applicationCoordinator: ApplicationCoordinator?
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        applicationCoordinator = ApplicationCoordinator(window: UIWindow(frame: windowScene.coordinateSpace.bounds))
        coordinator = applicationCoordinator?.handleUserState(windowScene: windowScene)
        coordinator?.start()
    }
}
